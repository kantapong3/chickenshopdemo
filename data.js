let products = [
  {
    id: 1,
    title: "Chicken Boiled",
    content: "ABCDEFG",
    image: "https://cdn.pixabay.com/photo/2019/01/23/10/35/fried-3949947_960_720.jpg",
    price: 30,
  },
  {
    id: 2,
    title: "Chicken Fried & salad",
    content: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
    image: "https://media.istockphoto.com/photos/homemade-southern-fried-chicken-picture-id477596181?b=1&k=20&m=477596181&s=170667a&w=0&h=MGulJ_s7h61YFQMlweRyqjsTkZgeI1EnF0IURj8a0xQ=",
    price: 45,
  },
  {
    id: 3,
    title: "Chicken fried",
    content: "ABCDEFG",
    image: "https://media.istockphoto.com/photos/plate-of-delectable-golden-brown-crispy-fried-chickens-on-wooden-picture-id1297771706?b=1&k=20&m=1297771706&s=170667a&w=0&h=ixHUdFPoep-xvHdKqSRme4iSE3vQPBXHo2slwON4wbM=",
    price: 40,
  },
  {
    id: 4,
    title: "Chicken and Rice",
    content: "ABCDEFG",
    image: "https://media.istockphoto.com/photos/fried-chicken-on-rice-picture-id1328613025?b=1&k=20&m=1328613025&s=170667a&w=0&h=D-Px_pIEv2fpID6Dfro48wmZrJ9rFeuhUQLR2pUjYYw=",
    price: 50,
  }
];

module.exports = function () {
  return {
      products: products,
  };
};
