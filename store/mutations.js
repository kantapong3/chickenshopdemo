export const SET_PRODUCTS = (state, products) => {
    state.products = products;
}
export const SET_PRODUCT = (state, product) => {
    state.product = product;
}
export const ADD_TO_CART = (state, {product, amount}) => {
    let productInCart = state.cart.find(item => {
        return item.product.id === product.id
    })

    if(productInCart){
        productInCart.amount += amount
        return
    }
    state.cart.push({
        product,
        amount
    })
}