import axios from "axios";
export const getProducts = ({ commit }) => {
  axios.get("http://localhost:3800/products").then(response => {
    commit("SET_PRODUCTS", response.data);
  });
};
export const getProduct = ({ commit }) => {
  axios.get("http://localhost:3800/products/${porductId}").then(response => {
    commit("SET_PRODUCT", response.data);
  })
};

export const addProductToCart = ({ commit }, { product, amount }) => {
  commit("ADD_TO_CART", { product, amount });
};
