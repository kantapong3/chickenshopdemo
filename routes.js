import Index from "@/pages/index"
import Product from "@/pages/product"

export default[
    {
        path: '/',
        component: Index,
        name: 'index'
    },
    {
        path: '/product/:id',
        component: Product,
        name: 'product',
        props: true
    }
]